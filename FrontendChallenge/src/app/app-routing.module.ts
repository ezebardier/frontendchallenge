import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DogsComponent } from './dogs/dogs.component';
import { HomeComponent } from './home/home.component';
import { TodoComponent } from './todo/todo.component';


const routes: Routes = [
  { path: 'dogs', component: DogsComponent },
  { path: 'todo', component: TodoComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

