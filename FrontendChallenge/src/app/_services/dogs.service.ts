import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class DogsService {
apiUrl=`${environment.apiUrl}`;
  
 constructor(private http: HttpClient) { }

  getBreedsAll(): Observable<any> {   
    return this.http.get<any>(this.apiUrl + 'breeds/list/all');      
  }

  getBreedImages(breed: string): Observable<any> {   
    return this.http.get<any>(this.apiUrl + `breed/${breed}/images`);      
  }
} 