import { Component, OnInit } from '@angular/core';
import { DogsService } from '../_services/dogs.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {
  
  breeds: string[] = new Array<string>();
  images: string[] = new Array<string>();
  selectedBreed: string;
  error: string;

  constructor(private dogsService: DogsService) { }

  ngOnInit() {
    this.loadBredds();
  }

  loadBredds(){
    this.dogsService.getBreedsAll()
      .subscribe(
        data => {          
          console.log(data.message);
          for (var key in data.message)
            if ( data.message[key] != null){ 
              this.breeds.push(key);
            }
        },
        error => {
          console.log(error);
        });          
  }

  onSubmit() {
    this.error="";
    if(!this.selectedBreed){
      this.error="Please select a breed.";
      return;
    } 
    this.dogsService.getBreedImages(this.selectedBreed)
      .subscribe(
        data => {       
          this.images =  new Array<string>();
          console.log(data.message);
          for (var key in data.message)
            if ( data.message[key] != null){ 
              // console.log(key + " " + data.message[key]);
              this.images.push(data.message[key]);
            }
        },
        error => {
          console.log(error);
        });  
               
  }
}
