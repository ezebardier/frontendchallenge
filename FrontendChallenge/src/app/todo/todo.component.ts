import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { Task } from '../_models/task';
import * as moment from 'moment';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  items: string[] = new Array<string>();
  taskList: Task[] = new Array<Task>();
  todoForm: FormGroup;
  submitted = false;
  @ViewChild('taskName', {static: false}) taskNameRef: ElementRef;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.todoForm = this.formBuilder.group({
      taskName: ["", Validators.required],
      date:["", Validators.compose([Validators.required, this.dateFormatValidator])]
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.todoForm.invalid) {
      return;
    }

    console.log('submit ' + this.todoForm.value['task']);
    this.items.push(this.todoForm.value['task'])
    this.taskList.push(this.todoForm.value)

    this.todoForm.reset();
    this.submitted = false;
    this.items.sort().reverse()
    this.taskNameRef.nativeElement.focus()

    this.taskList = this.taskList.sort(function(a, b){
      if(a.taskName < b.taskName) { return 1; }
      if(a.taskName > b.taskName) { return -1; }
      return 0;
    })
    console.log(this.taskList);
    
  }

  dateFormatValidator(control: FormControl): { [key: string]: any } {
    var passed = moment(control.value, 'MM/DD/YYYY', true).isValid();
    if (!passed) {
      return { invalidDate: true };
    }
    //console.log('date valid ' + passed);

    // var regexp = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;    
    // if (control.value && !regexp.test(control.value)) {
    //   return { invalidDate: true };
    // }    
  }

}
